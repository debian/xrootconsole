From: Julien Viard de Galbert <julien@silicone.homelinux.org>
Date: Wed, 1 Apr 2009 23:42:39 +0200
Subject: ansi-color

Origin: http://silicone.homelinux.org/2009/04/02/ansi-color-support-for-xrootconsole/

Slightly modified by Axel Beckert <abe@debian.org> to work together
with the UTF-8 support of the previous patch. Further changes were
made to make the test script suitable for build-time and as-installed
testing, too.

Later updated by Julien Vdg (original author)
  * Handle multiple SRG parameters
  * Fix c++ style comments
  * So now both colortail and lwatch should be supported.
  * Add --ansi-color to enable the feature
---
 main.c         | 364 ++++++++++++++++++++++++++++++++++++++++++++++++++++-----
 test           |   3 +
 xrootconsole.1 |   2 +
 3 files changed, 339 insertions(+), 30 deletions(-)

--- a/main.c
+++ b/main.c
@@ -10,6 +10,13 @@
  * $Id: main.c,v 1.13 2004/02/20 22:31:53 bob Exp $
  *
  *
+ * ANSI colors:      Copyright (C) 2009, 2010  Julien Viard de Galbert
+ * | ref: http://en.wikipedia.org/wiki/ANSI_escape_code
+ * |
+ * | TODO: allow customizing color table from cmd line ?
+ * |
+ * | usage: tail -f /var/log/messages | stdbuf -oL ccze -A | xrootconsole --ansi-color
+ *
  *
  * 
  **************************************************************************
@@ -70,6 +77,7 @@
 "  --topdown        insert lines at the top and scroll the rest down\n" \
 "  --bottomup       start inserting lines at the bottom\n" \
 "  --wrap           wrap long lines, instead of cutting them off\n" \
+"  --ansi-color     enable ANSI escape sequence parsing for color support\n"\
 "  -h or --help     a familiar-looking help screen\n" \
 "  -v or --version  displays the version number\n" \
 "  [console]        filename to read (defaults to standard input)\n" \
@@ -77,6 +85,52 @@
 "Report bugs to <bob@de-fac.to>.\n"
 
 
+#define ESC_SEQ_START       0x10000
+#define ESC_SEQ_CONT        0x20000
+#define ESC_SEQ_DATA_MASK   0xFFFF
+
+
+#define REND_DEFAULT        0
+#define REND_FG_COLOR_MASK  0x0007
+#define REND_FG_SET_COLOR   0x0008
+#define REND_FG_FULLCOLOR_MASK  (REND_FG_COLOR_MASK | REND_FG_SET_COLOR)
+#define REND_FG_COLOR(c)    (REND_FG_SET_COLOR + ((c) & REND_FG_COLOR_MASK))
+#define REND_FG_COLOR_GET(r) (r & REND_FG_COLOR_MASK)
+#define REND_BG_COLOR_MASK  0x0070
+#define REND_BG_SET_COLOR   0x0080
+#define REND_BG_FULLCOLOR_MASK  (REND_BG_COLOR_MASK | REND_BG_SET_COLOR)
+#define REND_BG_COLOR(c)    (REND_BG_SET_COLOR + (((c)<<4) & REND_BG_COLOR_MASK))
+#define REND_BG_COLOR_GET(r) ((r & REND_BG_COLOR_MASK)>>4)
+
+#define REND_BOLD           0x0100  /* used as fg high intensity */
+#define REND_BLINK          0x0200  /* used as bg high intensity */
+#define REND_ULINE          0x0400
+#define REND_RVIDEO         0x0800
+
+static const char *def_colorName[] =
+{
+/* low-intensity colors */
+    "Black",            /* 0: black             (#000000) */
+    "Red3",             /* 1: red               (#CD0000) */
+    "Green3",           /* 2: green             (#00CD00) */
+//  "Yellow3",          /* 3: yellow            (#CDCD00) */
+    "#AB5700",          /* 3: brown actually (EGA/DOS ANSY.SYS compatible) */
+    "Blue3",            /* 4: blue              (#0000CD) */
+    "Magenta3",         /* 5: magenta           (#CD00CD) */
+    "Cyan3",            /* 6: cyan              (#00CDCD) */
+    "AntiqueWhite",     /* 7: white             (#FAEBD7) */
+/* high-intensity colors */
+    "Grey25",           /* 8: bright black      (#404040) */
+    "Red",              /* 9: bright red        (#FF0000) */
+    "Green",            /* 10: bright green     (#00FF00) */
+    "Yellow",           /* 11: bright yellow    (#FFFF00) */
+    "Blue",             /* 12: bright blue      (#0000FF) */
+    "Magenta",          /* 13: bright magenta   (#FF00FF) */
+    "Cyan",             /* 14: bright cyan      (#00FFFF) */
+    "White",            /* 15: bright white     (#FFFFFF) */
+};
+
+
 #define DEFAULT_X        0
 #define DEFAULT_Y        0
 #define DEFAULT_COLS        80
@@ -90,6 +144,7 @@
 #define DEFAULT_WRAP     False
 #define DEFAULT_TOPDOWN  False
 #define DEFAULT_SOLID    False
+#define DEFAULT_ANSI     False
 
 
 typedef struct {
@@ -104,6 +159,7 @@
     int wrap;
     int topdown;
     int solid;
+    int ansi;
 } InitOptions;
 
 typedef struct {
@@ -128,15 +184,86 @@
     int nlines;           /* number of lines to keep in memory */
     int intercolgap;      /* width between two columns */
     XSetWindowAttributes swa;
+    unsigned long colortable[16];
+    unsigned long fgcolor;
+    unsigned long bgcolor;
 } WindowSettings;
 
 typedef struct {
     const char* console;   /* name of the console file */
     int fd;                /* file descriptor of the console file */
     char* text;            /* circular buffer to store the characters */
+    short* rend;           /* circular buffer to store the rendition options */
     int pos;               /* where the actual first line in text is */
+    int escapeseq;         /* we are in an escape sequence */
+    short currend;         /* current renditions options */
+    short tmprend;         /* temporary renditions options (to handle ; )*/
 } ConsoleSettings;
 
+/* decode SGR */
+static short select_graphic_rendition(short rendition, int code)
+{
+    switch(code) {
+        case 0:
+            rendition = REND_DEFAULT;
+            break;
+        case 1:
+            rendition |= REND_BOLD;
+            break;
+        case 4:
+            rendition |= REND_ULINE;
+            break;
+        case 5:
+            rendition |= REND_BLINK;
+            break;
+        case 7:
+            rendition |= REND_RVIDEO;
+            break;
+        case 22:
+            rendition &= ~REND_BOLD;
+            break;
+        case 24:
+            rendition &= ~REND_ULINE;
+            break;
+        case 25:
+            rendition &= ~REND_BLINK;
+            break;
+        case 27:
+            rendition &= ~REND_RVIDEO;
+            break;
+
+        case 30:
+        case 31:        /* set fg color */
+        case 32:
+        case 33:
+        case 34:
+        case 35:
+        case 36:
+        case 37:
+            rendition = (rendition & ~REND_FG_FULLCOLOR_MASK)
+                | REND_FG_COLOR(code-30);
+            break;
+        case 39:        /* default fg */
+            rendition = rendition & ~REND_FG_FULLCOLOR_MASK;
+            break;
+
+        case 40:
+        case 41:        /* set bg color */
+        case 42:
+        case 43:
+        case 44:
+        case 45:
+        case 46:
+        case 47:
+            rendition = (rendition & ~REND_BG_FULLCOLOR_MASK)
+                | REND_BG_COLOR(code-40);
+            break;
+        case 49:        /* default bg */
+            rendition = rendition & ~REND_BG_FULLCOLOR_MASK;
+            break;
+    }
+    return rendition;
+}
 
 /*
  * put
@@ -148,44 +275,183 @@
     const unsigned char* s = (const unsigned char*) st;
 
     while ( (len==-1)?(*s != '\0'):(len-->0) ) {
-        switch (*s) {
-        case '\n':
-            x = 0;
-            if (io->topdown) {
-                cs->pos = (cs->pos + ws->nlines - 1) % ws->nlines;
-                memset(cs->text + cs->pos * ws->colwidth, ' ', ws->colwidth);
+        /* For escape sequence we assume we got only supported commands
+           ie COLOR only */
+        /* really entering : ESC[ ? */
+        if (ESC_SEQ_START==cs->escapeseq) {
+            if (*s == '[') {
+                /* yes, continue with escape seq */
+                cs->escapeseq = ESC_SEQ_CONT;
+                /* handle ESC[m -> ESC[0m special case */
+                cs->tmprend = REND_DEFAULT;
             }
-            else if (y == ws->nlines - 1) {
-                memset(cs->text + cs->pos * ws->colwidth, ' ', ws->colwidth);
-                cs->pos = (cs->pos + 1) % ws->nlines;
+            else {
+                /* no fall back to normal string processing */
+                cs->escapeseq = False;
             }
-            else y++;
-            break;
-
-        case '\t':
-            x = (x / 8 + 1) * 8;
-            if (x >= ws->cols && io->wrap && *(s+1)!='\n') put("\n",1,io,ws,cs);
-            break;
+        }
 
-        default:
-            if (*s >= ' ' && x < ws->colwidth) {
-                cs->text[x + ((y + cs->pos) % ws->nlines) * ws->colwidth] = *s;
-                if (++x == ws->colwidth && io->wrap && *(s+1)!='\n') put("\n",1,io,ws,cs);
+        /* inside an escape sequence ? */
+        if (cs->escapeseq) {
+            switch (*s) {
+                case '[':
+                    break; /* already treated */
+                case '0':
+                case '1':
+                case '2':
+                case '3':
+                case '4':
+                case '5':
+                case '6':
+                case '7':
+                case '8':
+                case '9':
+                    cs->escapeseq = ESC_SEQ_CONT | ((cs->escapeseq & ESC_SEQ_DATA_MASK) * 10 + *s - '0');
+                    /* at least one char, so not ESC[m, behave like a modif */
+                    cs->tmprend = cs->currend;
+                    break;
+                case ';':
+                    cs->tmprend = select_graphic_rendition(cs->tmprend, cs->escapeseq & ESC_SEQ_DATA_MASK);
+                    break;
+                case 'm':
+                    cs->currend = select_graphic_rendition(cs->tmprend, cs->escapeseq & ESC_SEQ_DATA_MASK);
+                default:
+                    cs->escapeseq = False;
+            }
+        }
+        else {
+            switch (*s) {
+            case '\n':
+                if (io->topdown) {
+                    cs->pos = (cs->pos + ws->nlines - 1) % ws->nlines;
+                    memset(cs->text + cs->pos * ws->colwidth, ' ', ws->colwidth);
+                    if (io->ansi) {
+                        for(x = 0; x < ws->colwidth; x++) {
+                            cs->rend[cs->pos * ws->colwidth + x] = cs->currend;
+                        }
+                    }
+                }
+                else if (y == ws->nlines - 1) {
+                    memset(cs->text + cs->pos * ws->colwidth, ' ', ws->colwidth);
+                    if (io->ansi) {
+                        for(x = 0; x < ws->colwidth; x++) {
+                            cs->rend[cs->pos * ws->colwidth + x] = cs->currend;
+                        }
+                    }
+                    cs->pos = (cs->pos + 1) % ws->nlines;
+                }
+                else y++;
+                x = 0;
+                break;
+
+            case '\t':
+                x = (x / 8 + 1) * 8;
+                if (x >= ws->cols && io->wrap && *(s+1)!='\n') put("\n",1,io,ws,cs);
+                break;
+
+            case '\033': /* ESC */
+                /* enter escape sequence start mode */
+                if (io->ansi) cs->escapeseq = ESC_SEQ_START;
+                break;
+
+            default:
+                if (*s >= ' ' && x < ws->colwidth) {
+                    cs->text[x + ((y + cs->pos) % ws->nlines) * ws->colwidth] = *s;
+                    if (io->ansi) {
+                        cs->rend[x + ((y + cs->pos) % ws->nlines) * ws->colwidth] = cs->currend;
+                    }
+                    if (++x == ws->colwidth && io->wrap && *(s+1)!='\n') put("\n",1,io,ws,cs);
+                }
+                break;
             }
-            break;
         }
 
         s++;
     }
 }
 
+void draw_textline(InitOptions *io, WindowSettings *ws, ConsoleSettings *cs, int x, int y, int offset, int length)
+{
+    XGCValues values;
+    int len;
+    short rend = REND_DEFAULT;
+
+    while (length > 0) {
+        rend = cs->rend[offset];
+        len = 1;
+        /* search longer segment with same color */
+        while ((cs->rend[offset+len] == rend) && (len < length)) len ++;
+
+        if (rend & REND_FG_SET_COLOR) {
+            values.foreground = ws->colortable[REND_FG_COLOR_GET(rend) + ((rend & REND_BOLD)?8:0) ];
+        }
+        else {
+            values.foreground = ws->fgcolor;
+        }
+
+        //printf("len:%3d rend: %04x\n",len, rend);
+
+        if (rend & (REND_BG_SET_COLOR | REND_RVIDEO) ) {
+            if (rend & REND_RVIDEO) {
+                values.background = values.foreground;
+                if (rend & REND_BG_SET_COLOR) {
+                    values.foreground = ws->colortable[REND_BG_COLOR_GET(rend) + ((rend & REND_BLINK)?8:0) ];
+                }
+                else {
+                    if (io->solid == True) {
+                        values.foreground = ws->bgcolor;
+                    }
+                    else {
+                        /* transparent mode use AND so it make bg darker
+                         * just make the letters black, could be too dark,
+                         * but probably more readable */
+                        values.foreground = ws->colortable[0];
+                    }
+                }
+            }
+            else {
+                values.background = ws->colortable[REND_BG_COLOR_GET(rend) + ((rend & REND_BLINK)?8:0) ];
+            }
+            XChangeGC(ws->dpy, ws->foregc, GCForeground | GCBackground, &values);
+            XmbDrawImageString(ws->dpy, ws->buf, ws->fn, ws->foregc,
+                    x,
+                    y,
+                    cs->text + offset,
+                    len);
+        }
+        else {
+            XChangeGC(ws->dpy, ws->foregc, GCForeground, &values);
+            XmbDrawString(ws->dpy, ws->buf, ws->fn, ws->foregc,
+                    x,
+                    y,
+                    cs->text + offset,
+                    len);
+        }
+        if (rend & REND_BOLD) {
+            XmbDrawString(ws->dpy, ws->buf, ws->fn, ws->foregc,
+                    x+1,
+                    y,
+                    cs->text + offset,
+                    len);
+        }
+        length -= len;
+        offset += len;
+        x += len * ws->extent->max_logical_extent.width; /* can be error !!!! */
+    }
+
+    /* restore rendering options */
+    if(rend != REND_DEFAULT) {
+        values.foreground = ws->fgcolor;
+        XChangeGC(ws->dpy, ws->foregc, GCForeground, &values);
+    }
+}
 
 /*
  * write the text in the pixmap
  */
 void draw_pixmap(InitOptions *io, WindowSettings *ws, ConsoleSettings *cs) {
     int rowc = cs->pos + (io->topdown==True);    /* row in caracters */
-    int rowp = ws->font_ascent;        /* row in pixels */
+    int rowp = ws->font_ascent;                  /* row in pixels */
     int i,j;
 
     /* setup the background */
@@ -195,11 +461,17 @@
     /* write the string */
     for (i = ws->rows; i>0; --i) {
         for (j = 0; j < io->tc; ++j) {
-            XmbDrawString(ws->dpy, ws->buf, ws->fn, ws->foregc,
-                j * (ws->colwidth * ws->extent->max_logical_extent.width + ws->intercolgap),
-                rowp,
-                cs->text + ((rowc + j*(ws->rows)) % ws->nlines)*(ws->colwidth),
-                ws->colwidth);
+            int x, offset;
+            x=j * (ws->colwidth * ws->extent->max_logical_extent.width + ws->intercolgap);
+            offset=((rowc + j*(ws->rows)) % ws->nlines)*(ws->colwidth);
+            if (io->ansi) {
+                draw_textline(io, ws, cs,
+                    x, rowp, offset, ws->colwidth);
+            }
+            else {
+                XmbDrawString(ws->dpy, ws->buf, ws->fn, ws->foregc,
+                    x, rowp, cs->text + offset, ws->colwidth);
+            }
         }
         rowp += ws->font_ascent + ws->font_descent;
         ++rowc;
@@ -225,6 +497,7 @@
     io->wrap = DEFAULT_WRAP;
     io->topdown = DEFAULT_TOPDOWN;
     io->solid = DEFAULT_SOLID;
+    io->ansi = DEFAULT_ANSI;
 
     /* Process command-line arguments */
     while (*++argv != NULL) {
@@ -250,6 +523,10 @@
             io->topdown = True;
         else if (!strcmp(*argv, "--bottomup"))
             io->topdown = -1234;
+        else if (!strcmp(*argv, "--ansi-color"))
+            io->ansi = True;
+        else if (!strcmp(*argv, "--ansi-colour"))
+            io->ansi = True;
         else if (!strcmp(*argv, "--version") || !strcmp(*argv, "-v")) {
             fprintf(stderr, "xrootconsole %s\n", XROOTCONSOLE_VERSION);
             exit(EXIT_SUCCESS);
@@ -284,10 +561,14 @@
     if (cs->console == NULL) exit(EXIT_SUCCESS);
     close(cs->fd);
     cs->fd = -1;
+    cs->escapeseq = False;
+    cs->currend = REND_DEFAULT;
+    cs->tmprend = REND_DEFAULT;
 }
 
 void init_console(const char *console,
                     int colwidth, int nlines,
+                    int ansi,
                     ConsoleSettings *cs) {
 
     /* we never modify the name through a ConsoleSettings*,
@@ -298,9 +579,20 @@
        extra one is for the line being read. */
     cs->text = (char*)malloc(colwidth * nlines);
     memset(cs->text, ' ', colwidth * nlines);
+    /* Create rendering info array */
+    if (ansi) {
+        cs->rend = (short*)malloc(colwidth * nlines * sizeof(short));
+        memset(cs->rend, 0, colwidth * nlines * sizeof(short));
+    }
+    else {
+        cs->rend = NULL;
+    }
     cs->pos = 0;
     if (cs->console == NULL) cs->fd = 0;      /* STDIN special case, */
     else cs->fd = open_console(cs->console);  /* or open normal file */
+    cs->escapeseq = False;
+    cs->currend = REND_DEFAULT;
+    cs->tmprend = REND_DEFAULT;
 }
 
 
@@ -451,8 +743,20 @@
     ws->buf = XCreatePixmap(ws->dpy, ws->win, ws->width, ws->height,
             DefaultDepth(ws->dpy, DefaultScreen(ws->dpy)));
 
+    /* build colortable */
+    ws->fgcolor = load_color(io->fg, ws->dpy);
+    ws->bgcolor = load_color(io->bg, ws->dpy);
+
+    if (io->ansi) {
+        int i;
+        for(i = 0; i < 16; i ++)
+        {
+            ws->colortable[i] = load_color(def_colorName[i], ws->dpy);
+        }
+    }
+
     /* Create the foreground GC */
-    values.foreground = load_color(io->fg, ws->dpy);
+    values.foreground = ws->fgcolor;
     values.graphics_exposures = 0;
     ws->foregc = XCreateGC(ws->dpy, ws->win,
            GCForeground | GCGraphicsExposures,
@@ -460,7 +764,7 @@
 
     /* initialize the background pixmap */
     values.background = values.foreground;
-    values.foreground = load_color(io->bg, ws->dpy);
+    values.foreground = ws->bgcolor;
     if (io->solid == True) {
 
         XLowerWindow(ws->dpy, ws->win);
@@ -599,7 +903,7 @@
 
     init_options(argv, &io);
     init_window(&io,&ws);
-    init_console(io.console_name, ws.colwidth, ws.nlines, &cs);
+    init_console(io.console_name, ws.colwidth, ws.nlines, io.ansi, &cs);
 
     /* Display a message */
     put("xrootconsole ",-1,&io,&ws,&cs);
--- a/test
+++ b/test
@@ -25,6 +25,10 @@
 timeout 7s sh -c "{ z=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx; while [ 1 ] ; do sleep 1; z=x$z; echo $z; done; } |./xrootconsole --bottomup -bw 2 -bd '#000000' -fg '#FF3333' --wrap -geometry +100+260 $@"
 LRC=$?; if [ "$LRC" -ne 124 ]; then RC="$LRC"; echo Test failed 1>&2; fi
 
+#ansi color test
+timeout 7s sh -c "/bin/echo -ne '\e[mNormal \e[30mBlack \e[31mRed \e[32mGreen \e[33mBrown  \e[34mBlue \e[35mMagena \e[36mCyan \e[37mWhite \e[39mNormal\n\e[1mBold   \e[30mBlack \e[31mRed \e[32mGreen \e[33mYellow \e[34mBlue \e[35mMagena \e[36mCyan \e[37mWhite \e[39mNormal\n\e[mNormal \e[40mBlack \e[41mRed \e[42mGreen \e[43mBrown  \e[44mBlue \e[45mMagena \e[46mCyan \e[47mWhite \e[49mNormal\n\e[5mBlink  \e[40mBlack \e[41mRed \e[42mGreen \e[43mYellow \e[44mBlue \e[45mMagena \e[46mCyan \e[47mWhite \e[49mNormal\n\e[7mInvert' | ./xrootconsole --ansi-color -fg '#9966FF' -geometry +100+260 $@"
+LRC=$?; if [ "$LRC" -ne 124 ]; then RC="$LRC"; echo Test failed 1>&2; fi
+
 if [ "$RC" -eq 0 ]; then
     echo "All tests succeeded."
     exit 0
--- a/xrootconsole.1
+++ b/xrootconsole.1
@@ -45,6 +45,8 @@
 .It Fl -wrap
 Rather than truncating long lines, this option specifies that xrootconsole
 should wrap excess text to the following line or lines, as necessary.
+.It Fl -ansi-color
+Enable color support by parsing ANSI escape sequences.
 .It Fl geometry Ar geometry
 This option specifies the preferred size and position of the window; see
 .Xr X 1 .
