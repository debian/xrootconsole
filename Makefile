# vi:set ts=4 sw=4 et:
#
# Makefile
# xrootconsole
# Copyright (C) 1988,1999   Eric Youngblut & Bob Galloway
#
# $Id: Makefile,v 1.4 2004/02/20 21:53:28 bob Exp $
#
# 
###########################################################################
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# The author of the original version of this software can be reached
# by e-mail at yngblut@cs.washington.edu.  The original version of
# this software can be found at http://www.cs.washington.edu/homes/yngblut/
#
# The author of the current version of this software can be reached
# by e-mail at bgallowa@wso.williams.edu.  The latest version of this
# software can be found at http://wso.williams.edu/~bgallowa/ 
#
###########################################################################

EXEC = xrootconsole
BINDIR = /usr/local/bin
MANFILE = xrootconsole.1
MANDIR = /usr/local/share/man/man1

VERSION = $(shell tag=$$(echo '$$Name: RELEASE-0_6 $$' | sed -e 's/\$$//g' -e 's/Name://g'); \
        if [ $$tag ]; \
            then echo $$tag; \
            else echo Bleeding Edge Version; \
        fi;)

CC = gcc
CPPFLAGS =

# for production
CFLAGS = -O2 -DNDEBUG -Wall "-DXROOTCONSOLE_VERSION=\"$(VERSION)\"" \
    -I /usr/X11R6/include
LDFLAGS = -s

# for testing
#CFLAGS = -g -Wall "-DXROOTCONSOLE_VERSION=\"$(VERSION) (debug build)\"" \
    -I /usr/X11R6/include
#LDFLAGS = 

LIBS = -L/usr/X11R6/lib -lX11 -lutil

OBJS = main.o util.o


# targets -----------------------------------------------------------------

all: $(EXEC)

$(EXEC): $(OBJS) Makefile
	$(CC) $(LDFLAGS) $(OBJS) $(LIBS) -o $@

$(OBJS): %.o: %.c util.h Makefile

clean:
	rm -f *.o core *~
realclean:
	rm -f $(EXEC) *.o core *~

install: $(EXEC)
	cp $(EXEC) $(BINDIR)
	cp $(MANFILE) $(MANDIR)
