/* vi:set ts=4 sw=4 et:
 *
 * util.h
 * xrootconsole
 * Original version: Copyright (C) 1998, 1999  Eric Youngblut
 * 
 * 
 * $Id: util.h,v 1.3 2000/10/10 02:17:53 bob Exp $
 *
 *
 *
 **************************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * The author of the original version of this software can be reached
 * by e-mail at yngblut@cs.washington.edu.  The original version of
 * this software can be found at http://www.cs.washington.edu/homes/yngblut/
 *
 * The author of the current version of this software can be reached
 * by e-mail at bgallowa@wso.williams.edu.  The latest version of this
 * software can be found at http://wso.williams.edu/~bgallowa/ 
 **************************************************************************/


#ifndef __XRC_UTIL_H
#define __XRC_UTIL_H


#include <X11/Xlib.h>



XFontStruct* load_font(const char* s, Display *dpy);
unsigned long load_color(const char* s, Display *dpy);



#endif
