/* vi:set ts=4 sw=4 et:
 *
 * util.c
 * xrootconsole
 * Original version: Copyright (C) 1998, 1999  Eric Youngblut
 *
 * 
 * $Id: util.c,v 1.4 2004/02/20 22:07:42 bob Exp $
 *
 *
 *
 * 
 **************************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * The author of the original version of this software can be reached
 * by e-mail at yngblut@cs.washington.edu.  The original version of
 * this software can be found at http://www.cs.washington.edu/homes/yngblut/
 *
 * The author of the current version of this software can be reached
 * by e-mail at bgallowa@wso.williams.edu.  The latest version of this
 * software can be found at http://wso.williams.edu/~bgallowa/ 
 **************************************************************************/


#include "util.h"
#include <stdio.h>
#include <stdlib.h>




XFontStruct* load_font(const char* s, Display *dpy) {
    XFontStruct* f = XLoadQueryFont(dpy, s);
    if (f == NULL) {
        fprintf(stderr, "Unable to open font");
        exit(EXIT_FAILURE);
    }
    return f;
}


unsigned long load_color(const char* s, Display *dpy) {
    XColor ce;
    Colormap colormap = DefaultColormap(dpy, DefaultScreen(dpy));

    if (XParseColor(dpy, colormap, s, &ce)) {
        if (XAllocColor(dpy, colormap, &ce)) return ce.pixel;
	
        fprintf(stderr, "Warning: could not allocate color\n");
        return WhitePixel(dpy, DefaultScreen(dpy));
    }

    fprintf(stderr, "Warning: could not parse color\n");
    return WhitePixel(dpy, DefaultScreen(dpy));
}
